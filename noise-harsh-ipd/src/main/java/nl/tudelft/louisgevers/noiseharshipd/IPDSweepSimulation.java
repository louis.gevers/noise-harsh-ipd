package nl.tudelft.louisgevers.noiseharshipd;

import sim.util.sweep.ParameterSweep;

import java.io.IOException;

/**
 * Class to run a parameter sweep simulation on the IPDSimulation class.
 * This class is just for convenience on the command line. ParameterSweep
 * can be called directly with supplied file if needed.
 */
public class IPDSweepSimulation {

    /**
     * Runs Mason's ParameterSweep on a fixed file. Used for convenience on the command line.
     * @param args Command line arguments
     * @throws IOException If ParameterSweep throws an exception
     * @throws ClassNotFoundException If ParameterSweep throws an exception
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ParameterSweep.main(new String[] {"exp2.params"});
    }

}
