package nl.tudelft.louisgevers.noiseharshipd.agents;

import nl.tudelft.louisgevers.noiseharshipd.IPDSimulation;
import nl.tudelft.louisgevers.noiseharshipd.environment.Grid;
import nl.tudelft.louisgevers.noiseharshipd.strategies.Strategy;
import nl.tudelft.louisgevers.noiseharshipd.util.Position;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;

/**
 * Represents a Player agent in the spatial harsh IPD game.
 */
public class Player implements Steppable {

    /**
     * 2 dimensional position of the player.
     */
    public final Position position;

    /**
     * IPD strategy used by the player.
     */
    public Class<? extends Strategy> strategy;

    /**
     * Whether the player has played that round.
     */
    public boolean played;

    /**
     * Energy of the player.
     */
    private double energy;

    /**
     * Whether the player has cooperated on the last move.
     */
    public boolean cooperated;

    /**
     * Creates the player with given strategy.
     */
    public Player(Class<? extends Strategy> strategy) {
        this.strategy = strategy;
        this.position = new Position(0, 0);
        this.energy = 0;
        this.played = false;
        this.cooperated = false;
    }

    /**
     * Called for every step of the simulation. Represents one round of the agent.
     * @param state IPD simulation state class
     */
    @Override
    public void step(SimState state) {
        if (!this.played) {
            IPDSimulation ipd = (IPDSimulation) state;
            Grid grid = ipd.grid;
            // Only play against players that haven't played yet
            Bag neighborhood = grid.getNeighbors(this);
            neighborhood.removeIf(o -> {
                Player other = (Player) o;
                return other.played;
            });
            // If neighborhood is empty, move randomly
            if (neighborhood.isEmpty()) {
                grid.randomMove(this, ipd.random);
                ipd.costOfLife(this);
            } else {
                int index = ipd.random.nextInt(neighborhood.size());
                Player other = (Player) neighborhood.get(index);
                ipd.playGame(this, other);
            }
        }
    }

    /**
     * Add the given amount to the energy reserve.
     * @param amount Amount to add
     */
    public void addEnergy(double amount) {
        this.energy += amount;
    }

    /**
     * Remove the given amount from the energy reserve.
     * @param amount Amount to remove
     */
    public void removeEnergy(double amount) {
        this.energy -= amount;
    }

    /**
     * Get the current energy level.
     * @return The current energy level
     */
    public double getEnergy() {
        return this.energy;
    }

    /**
     * Get the name of the Strategy the player's using.
     * @return The name of the strategy of the player
     */
    public String getStrategyName() {
        return strategy.getSimpleName();
    }
}
