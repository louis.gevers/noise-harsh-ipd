package nl.tudelft.louisgevers.noiseharshipd.strategies;

/**
 * Spatial Generous Tit for Tat strategy. More generous
 * than classic GTFT.
 */
public class SGTFT extends GTFT {

    @Override
    public double getGenerosity() {
        return 0.6;
    }
}
