package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Tit for Tat strategy that only defects after two consecutive defections.
 */
public class TFTT extends Strategy {

    /**
     * Remember previous action for consecutive moves.
     */
    private Action lastAction;

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        Action action;
        if (lastAction != null && otherLastMove != null) {
            if (lastAction.equals(Action.DEFECT) && otherLastMove.equals(Action.DEFECT)) {
                action = Action.DEFECT;
            } else {
                action = Action.COOPERATE;
            }
        } else {
            action = Action.COOPERATE;
        }
        lastAction = otherLastMove;
        return action;
    }

}
