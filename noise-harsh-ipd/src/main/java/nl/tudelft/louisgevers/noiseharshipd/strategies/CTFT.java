package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Contrite Tit for Tat strategy.
 */
public class CTFT extends Strategy {

    /**
     * Represent the states the strategy can be in.
     */
    private enum State {
        CONTRITE,
        CONTENT,
        PROVOKED
    }

    /**
     * State of the strategy.
     */
    private State state;

    /**
     * Initializes the strategy in content state.
     */
    public CTFT() {
        this.state = State.CONTENT;
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (lastMove == null) {
            return Action.COOPERATE;
        } else {
            if (lastMove.equals(Action.COOPERATE) && otherLastMove.equals(Action.DEFECT) && state.equals(State.CONTENT)) {
                this.state = State.PROVOKED;
            } else if (lastMove.equals(Action.DEFECT) && otherLastMove.equals(Action.COOPERATE) && state.equals(State.CONTENT)) {
                this.state = State.CONTRITE;
            } else if (otherLastMove.equals(Action.COOPERATE) && state.equals(State.PROVOKED)) {
                this.state = State.CONTENT;
            } else if (lastMove.equals(Action.COOPERATE) && otherLastMove.equals(Action.COOPERATE) && state.equals(State.CONTRITE)) {
                this.state = State.CONTENT;
            }

            if (state.equals(State.PROVOKED)) {
                return Action.DEFECT;
            } else {
                return Action.COOPERATE;
            }
        }
    }
}
