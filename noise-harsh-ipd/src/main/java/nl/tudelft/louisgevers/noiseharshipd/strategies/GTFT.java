package nl.tudelft.louisgevers.noiseharshipd.strategies;

import ec.util.MersenneTwisterFast;
import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Generous Tit for Tat strategy.
 */
public class GTFT extends TFT {

    /**
     * Random number generator.
     */
    private MersenneTwisterFast random;

    /**
     * Initializes random number generator.
     */
    public GTFT() {
        this.random = new MersenneTwisterFast();
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (otherLastMove == null) {
            return Action.COOPERATE;
        } else {
            if (otherLastMove.equals(Action.DEFECT) && random.nextDouble() < getGenerosity()) {
                return Action.COOPERATE;
            } else {
                return otherLastMove;
            }
        }
    }

    /**
     * Get the generosity of the strategy. I.e. the probability it will cooperate
     * while it would otherwise defect.
     * @return The generosity of the strategy
     */
    public double getGenerosity() {
        return 0.1;
    }

}
