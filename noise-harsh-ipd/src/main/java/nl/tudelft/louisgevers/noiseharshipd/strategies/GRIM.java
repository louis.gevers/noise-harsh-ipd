package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Start with coop, always defect after first opponent defect.
 */
public class GRIM extends Strategy {

    /**
     * Whether to always defect.
     */
    private boolean angry;

    /**
     * Initializes cooperative behavior.
     */
    public GRIM() {
        this.angry = false;
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (angry) {
            return Action.DEFECT;
        } else {
            if (otherLastMove != null && otherLastMove.equals(Action.DEFECT)) {
                angry = true;
                return Action.DEFECT;
            }
        }
        return Action.COOPERATE;
    }
}
