package nl.tudelft.louisgevers.noiseharshipd.strategies;

import ec.util.MersenneTwisterFast;
import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Random strategy.
 */
public class RAND extends Strategy {

    /**
     * Random number generator.
     */
    private MersenneTwisterFast random;

    /**
     * Initializes the random number generator.
     */
    public RAND() {
        this.random = new MersenneTwisterFast();
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (random.nextDouble() < 0.5) {
            return Action.COOPERATE;
        } else {
            return Action.DEFECT;
        }
    }
}
