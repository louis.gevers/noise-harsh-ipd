package nl.tudelft.louisgevers.noiseharshipd;

import nl.tudelft.louisgevers.noiseharshipd.agents.Player;
import nl.tudelft.louisgevers.noiseharshipd.agents.Round;
import nl.tudelft.louisgevers.noiseharshipd.environment.Grid;
import nl.tudelft.louisgevers.noiseharshipd.game.Game;
import nl.tudelft.louisgevers.noiseharshipd.strategies.StrategiesCollection;
import nl.tudelft.louisgevers.noiseharshipd.strategies.Strategy;
import nl.tudelft.louisgevers.noiseharshipd.util.Parameters;
import nl.tudelft.louisgevers.noiseharshipd.util.Position;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import sim.engine.SimState;
import sim.util.Bag;

import java.lang.reflect.InvocationTargetException;
import java.util.*;


/**
 * Top-level simulation object representing the harsh spatial IPD game.
 */
public class IPDSimulation extends SimState {

    /**
     * Grid on which the agents live.
     */
    public Grid grid;

    /**
     * List of players.
     */
    private List<Player> players;

    /**
     * Maps players to their games.
     */
    private MultiKeyMap<Player, Game> games;

    /**
     * Round object where every player plays once in random order.
     */
    private Round round;

    /**
     * Collection of strategies that can be used.
     */
    public final StrategiesCollection strategies;

    /**
     * Parameters of the simulation.
     */
    public final Parameters parameters;

    /**
     * Count of strategies in the game.
     */
    protected final Map<String, Integer> strategiesCount;

    /**
     * Creates a spatial harsh IPD simulation.
     * @param seed Seed for the random number generator
     */
    public IPDSimulation(long seed) {
        super(seed);
        this.parameters = new Parameters();
        this.grid = new Grid(parameters);
        this.players = new ArrayList<>(parameters.getN());
        this.games = new MultiKeyMap<>();
        this.strategies = new StrategiesCollection(parameters.getStrategies());
        this.strategiesCount = new HashMap<>();
    }

    /**
     * Creates a spatial harsh IPD simulation with supplied parameters.
     * @param seed Seed for the random number generator
     * @param parameters Parameters for the simulation
     */
    public IPDSimulation(long seed, Parameters parameters) {
        super(seed);
        this.parameters = parameters;
        this.grid = new Grid(parameters);
        this.players = new ArrayList<>(parameters.getN());
        this.games = new MultiKeyMap<>();
        this.strategies = new StrategiesCollection(parameters.getStrategies());
        this.strategiesCount = new HashMap<>();
    }

    /**
     * Resets and initializes the simulation.
     */
    @Override
    public void start() {
        super.start();
        this.games = new MultiKeyMap<>();
        initializeAgents();
        initializeEnvironment();
    }

    /**
     * Create or resume an IPD game between the two given players.
     * @param player1 First player to play
     * @param player2 Second player to play
     */
    public void playGame(Player player1, Player player2) {
        Game game;
        if (games.containsKey(player1, player2)) {
            game = games.get(player1, player2);
        } else if (games.containsKey(player2, player1)) {
            game = games.get(player2, player1);
        } else {
            try {
                game = new Game(parameters, player1, player2, random);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
                finish();
                return;
            }
            games.put(player1, player2, game);
        }
        try {
            game.play();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
            finish();
        }
        costOfLife(player1);
        costOfLife(player2);
    }

    /**
     * Remove cost of life from the given player's energy and kill the player
     * if the energy is below 0. Reproduce if possible, and cap energy to
     * maximum amount.
     * @param player Player to apply the cost of life on
     */
    public void costOfLife(Player player) {
        player.removeEnergy(parameters.getK());

        final int minEnergy = 0;
        final int minReproductionEnergy = 100;
        final int reproductionCost = 50;
        final int maxEnergy = 150;

        if (player.getEnergy() > maxEnergy) {
            player.removeEnergy(player.getEnergy() - maxEnergy);
        }

        if (player.getEnergy() <= minEnergy) {
            kill(player);
        } else {
            if (this.players.size() < parameters.getMaxN() && player.getEnergy() >= minReproductionEnergy) {
                reproduce(player, reproductionCost);
            }
            if (parameters.getEvolution()) {
                evolve(player);
            } else if (this.players.size() == parameters.getMaxN()) {
                finish();
            }
        }
    }

    /**
     * Initialize agents of the simulation.
     */
    private void initializeAgents() {
        this.strategiesCount.clear();
        this.players.clear();
        for (int i = 0; i < parameters.getN(); i++) {
            Class<? extends Strategy> strategy = strategies.getStrategies().get(i % strategies.getStrategies().size());
            Player player = new Player(strategy);
            player.addEnergy(random.nextInt(50) + 1);
            players.add(player);
            final int count = strategiesCount.getOrDefault(player.getStrategyName(), 0);
            strategiesCount.put(player.getStrategyName(), count + 1);
        }
        round = new Round(players);
        schedule.scheduleRepeating(round);
    }

    /**
     * Initialize the environment of the simulation.
     */
    private void initializeEnvironment() {
        grid.clear();
        this.players.forEach((player -> grid.setAtRandomLocation(player, random)));
    }

    /**
     * Remove the given player from the game.
     * @param player Player to remove
     */
    private void kill(Player player) {
        this.players.remove(player);
        List<MultiKey<? extends Player>> toRemoveGameKeys = new ArrayList<>();
        this.games.mapIterator().forEachRemaining(multiKey -> {
            if (multiKey.getKey(0).equals(player) || multiKey.getKey(1).equals(player)) {
                toRemoveGameKeys.add(multiKey);
            }
        });
        toRemoveGameKeys.forEach(multiKey -> {
            this.games.remove(multiKey);
        });
        round.removeSteppable(player);
        grid.set(player.position.x, player.position.y, null);
        final int count = strategiesCount.get(player.getStrategyName());
        strategiesCount.replace(player.getStrategyName(), count - 1);
        if (this.players.isEmpty()) {
            this.finish();
        }
    }

    /**
     * Create a new player with same strategy on a random adjacent cell, if
     * such a cell is available.
     * @param player Player that is reproducing
     */
    private void reproduce(Player player, int reproductionCost) {
        Position newPosition = grid.getRandomAvailableCell(player, random);
        if (newPosition != null) {
            Player offspring = new Player(player.strategy);
            this.players.add(offspring);
            player.removeEnergy(reproductionCost);
            offspring.addEnergy(reproductionCost);
            round.addSteppable(offspring);
            offspring.position.x = newPosition.x;
            offspring.position.y = newPosition.y;
            grid.set(offspring.position.x, offspring.position.y, offspring);
            final int count = strategiesCount.getOrDefault(offspring.getStrategyName(), 0);
            strategiesCount.put(offspring.getStrategyName(), count + 1);
        }
    }

    private void evolve(Player player) {
        Bag neighborhood = grid.getNeighbors(player);
        if (!neighborhood.isEmpty()) {
            neighborhood.shuffle(random);
            Optional<Object> highestEnergyNeighbor = Arrays.stream(neighborhood.toArray()).max((p1, p2) -> {
                Player player1 = (Player) p1;
                Player player2 = (Player) p2;
                return Double.compare(player1.getEnergy(), player2.getEnergy());
            });
            if (highestEnergyNeighbor.isPresent()) {
                Player neighbor = (Player) highestEnergyNeighbor.get();
                if (neighbor.getEnergy() > player.getEnergy() && !neighbor.strategy.equals(player.strategy)) {
                    final int countOld = strategiesCount.get(player.getStrategyName());
                    final int countNew = strategiesCount.get(neighbor.getStrategyName());
                    strategiesCount.replace(player.getStrategyName(), countOld - 1);
                    strategiesCount.replace(neighbor.getStrategyName(), countNew + 1);
                    player.strategy = neighbor.strategy;
//                    if (strategiesCount.get(neighbor.getStrategyName()).equals(parameters.getMaxN())) {
//                        finish();
//                    }
                }
            }
        }
    }

    /**
     * Returns the cooperation frequency of the game, used for inspections.
     * @return The cooperation frequency of the game
     */
    public double getTotalCooperationFrequency() {
        return round == null ? 0 : round.getCooperationFrequency();
    }

    /**
     * Returns the cooperation frequency of the last round. Used for inspection.
     * @return Cooperation frequency of the last round
     */
    public double getRoundCooperationFrequency() {
        return round == null ? 0 : round.getRoundCooperationFrequency();
    }

    /**
     * Returns the cost of life. Used for inspection.
     * @return Cost of life
     */
    public double getK() {
        return parameters.getK();
    }

    /**
     * Sets the cost of life. Used for parameter sweeping.
     * @param k Cost of life
     */
    public void setK(double k) {
        parameters.setK(k);
    }

    /**
     * Returns the sucker's payoff. Used for inspection.
     * @return  Sucker's payoff
     */
    public double getS() {
        return parameters.getS();
    }

    /**
     * Set the sucker's payoff. Used for parameter sweeping.
     * @param s Sucker's payoff
     */
    public void setS(double s) {
        parameters.setS(s);
    }

    /**
     * Returns the noise level. Used for inspection.
     * @return Noise level
     */
    public double getE() {
       return parameters.getE();
    }

    /**
     * Sets the noise level. Used for parameter sweeping.
     * @param e Noise level
     */
    public void setE(double e) {
        parameters.setE(e);
    }

    public boolean getEvolution() {
        return parameters.getEvolution();
    }

    public void setEvolution(boolean evolution) {
        parameters.setEvolution(evolution);
    }

    /**
     * Run the spatial harsh IPD simulation.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        doLoop(IPDSimulation.class, args);
        System.exit(0);
    }
}
