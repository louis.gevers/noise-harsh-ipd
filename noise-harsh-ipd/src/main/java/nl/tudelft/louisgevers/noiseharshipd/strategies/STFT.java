package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Tit for Tat strategy starting with defect.
 */
public class STFT extends Strategy {

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (otherLastMove == null) {
            return Action.DEFECT;
        } else {
            return otherLastMove;
        }
    }
}
