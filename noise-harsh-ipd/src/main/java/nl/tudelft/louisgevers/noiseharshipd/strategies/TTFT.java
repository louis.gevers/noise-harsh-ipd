package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Tit for Tat strategy but retaliates each defection with two defections.
 */
public class TTFT extends Strategy {

    /**
     * Whether to retaliate a second time.
     */
    private boolean retaliate;

    /**
     * Initializes the strategy with no initial retaliation.
     */
    public TTFT() {
        retaliate = false;
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (otherLastMove == null) {
            return Action.COOPERATE;
        } else if (retaliate) {
            retaliate = false;
            return Action.DEFECT;
        } else if (otherLastMove.equals(Action.DEFECT)) {
            retaliate = true;
            return Action.DEFECT;
        } else {
            return Action.COOPERATE;
        }
    }
}
