package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Always defecting strategy.
 */
public class ALLD extends Strategy {

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        return Action.DEFECT;
    }

}
