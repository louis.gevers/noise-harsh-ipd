package nl.tudelft.louisgevers.noiseharshipd.strategies;

import ec.util.MersenneTwisterFast;
import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Generous Pavlov strategy.
 */
public class GPavlov extends Pavlov {

    /**
     * Random number generator.
     */
    private MersenneTwisterFast random;

    /**
     * Initializes strategy with random number generator.
     */
    public GPavlov() {
        super();
        random = new MersenneTwisterFast();
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        Action action = super.play(lastMove, otherLastMove);
        if (action.equals(Action.DEFECT) && random.nextDouble() < getGenerosity()) {
            return Action.COOPERATE;
        } else {
            return action;
        }
    }

    public double getGenerosity() {
        return 0.1;
    }
}
