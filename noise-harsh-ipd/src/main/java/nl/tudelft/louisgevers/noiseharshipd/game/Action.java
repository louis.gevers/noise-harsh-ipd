package nl.tudelft.louisgevers.noiseharshipd.game;

/**
 * Represents possible actions in a Prisoner's Dilemma game.
 */
public enum Action {
    DEFECT,
    COOPERATE
}
