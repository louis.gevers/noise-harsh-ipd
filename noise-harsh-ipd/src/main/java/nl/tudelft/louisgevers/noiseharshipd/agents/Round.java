package nl.tudelft.louisgevers.noiseharshipd.agents;

import nl.tudelft.louisgevers.noiseharshipd.util.CumulativeAverage;
import sim.engine.RandomSequence;
import sim.engine.SimState;

import java.util.Collection;

/**
 * Represents a single Round in which each player plays in random order.
 */
public class Round extends RandomSequence {

    /**
     * Frequency of cooperation over all rounds.
     */
    private CumulativeAverage cooperationFrequency;

    /**
     * Frequency of cooperation of the last round.
     */
    private CumulativeAverage roundCooperativeFrequency;

    /**
     * Constructs the round as a random sequence.
     * @param steps Collection of players to add
     */
    public Round(Collection<Player> steps) {
        super(steps);
        this.cooperationFrequency = new CumulativeAverage();
    }

    @Override
    public void step(SimState state) {
        loadSteps();
        super.step(state);
        roundCooperativeFrequency = new CumulativeAverage();
        for (int i = 0; i < this.size; i++) {
            Player player = (Player) this.steps[i];
            // Reset played after round
            player.played = false;
            // Compute frequencies
            if (player.cooperated) {
                cooperationFrequency.add(1);
                roundCooperativeFrequency.add(1);
            } else {
                cooperationFrequency.add(0);
                roundCooperativeFrequency.add(0);
            }
        }
    }

    /**
     * Get the frequency of cooperation over all rounds.
     * @return The frequency of cooperation over all rounds
     */
    public double getCooperationFrequency() {
        return cooperationFrequency.getAverage();
    }

    /**
     * Get the frequency of cooperation of the last round.
     * @return The frequency of cooperation of the last round
     */
    public double getRoundCooperationFrequency() {
        return roundCooperativeFrequency == null ? 0 : roundCooperativeFrequency.getAverage();
    }
}
