package nl.tudelft.louisgevers.noiseharshipd.util;

/**
 * Helper class to compute a cumulative average.
 */
public class CumulativeAverage {

    /**
     * Computed average.
     */
    private double average;

    /**
     * Number of the next element.
     */
    private double i;

    /**
     * Initializes the Cumulative Average object.
     */
    public CumulativeAverage() {
        this.average = 0;
        this.i = 1;
    }

    /**
     * Add the given amount to the cumulative average.
     * @param x Amount to add to the average
     */
    public void add(int x) {
        if (i > 1) {
            average = (i - 1) / i * average + x / i;
        } else {
            average = x;
        }
        i++;
    }

    /**
     * Get the computed cumulative average.
     * @return Current average
     */
    public double getAverage() {
        return average;
    }

}
