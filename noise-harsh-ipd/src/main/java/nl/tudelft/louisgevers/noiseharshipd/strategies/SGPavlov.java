package nl.tudelft.louisgevers.noiseharshipd.strategies;

public class SGPavlov extends GPavlov {

    @Override
    public double getGenerosity() {
        return 0.6;
    }
}
