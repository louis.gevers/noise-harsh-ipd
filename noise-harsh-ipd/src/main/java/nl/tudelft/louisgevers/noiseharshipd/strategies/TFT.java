package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Tit for Tat strategy.
 */
public class TFT extends Strategy {


    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (otherLastMove == null) {
            return Action.COOPERATE;
        } else {
            return otherLastMove;
        }
    }
}
