package nl.tudelft.louisgevers.noiseharshipd.visuals;

import nl.tudelft.louisgevers.noiseharshipd.agents.Player;
import nl.tudelft.louisgevers.noiseharshipd.strategies.StrategiesCollection;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.RectanglePortrayal2D;

import java.awt.*;

/**
 * Responsible for visualizing a single player. Each player is represented
 * by a square with a color corresponding to its strategy.
 */
public class PlayerPortrayal extends RectanglePortrayal2D {

    /**
     * Strategies collection to determine colors.
     */
    private StrategiesCollection strategies;

    /**
     * Create player portrayal with given strategies collection.
     * @param strategies Collection of strategies with color mapping
     */
    public PlayerPortrayal(StrategiesCollection strategies) {
        this.strategies = strategies;
    }

    @Override
    public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
        Player player = (Player) object;
        paint = strategies.getColor(player.strategy);
        super.draw(object, graphics, info);
    }
}
