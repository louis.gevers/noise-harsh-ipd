package nl.tudelft.louisgevers.noiseharshipd.util;

import nl.tudelft.louisgevers.noiseharshipd.strategies.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class to hold parameters for simulations.
 */
public class Parameters {

    /**
     * Grid size.
     */
    private final int l;

    /**
     * Number of agents to initialize the grid with.
     */
    private final int n;

    /**
     * Maximum number of agents allowed on the grid.
     */
    private final int maxN;

    /**
     * Cost of living, subtracted from energy supplies for every player
     * at every round.
     */
    private double k;

    /**
     * Error rate. Probability to make opposite choice.
     */
    private double e;

    /**
     * Temptation payoff.
     */
    private final double t;

    /**
     * Reward payoff.
     */
    private final double r;

    /**
     * Punishment payoff.
     */
    private final double p;

    /**
     * Sucker's payoff.
     */
    private double s;

    /**
     * Strategies to use in the simulation.
     */
    private final List<Class<? extends Strategy>> strategies;

    /**
     * Whether evolution occurs.
     */
    private boolean evolution;

    /**
     * Create Parameters object with default parameters.
     */
    public Parameters() {
        this.l = 100;
        this.n = (int) (0.1 * Math.pow(this.l, 2));
        this.maxN = (int) (Math.pow(this.l, 2));
        this.k = 0.5;
        this.e = 0.1;
        this.t = 5;
        this.r = 3;
        this.p = 0;
        this.s = -2;
        this.evolution = false;
        this.strategies = new ArrayList<>(Arrays.asList(
                ALLC.class,
                ALLD.class,
//                CTFT.class,
//                GPavlov.class,
                SGPavlov.class,
                GRIM.class,
//                GTFT.class,
//                SGTFT.class,
                Pavlov.class,
                RAND.class,
                STFT.class,
//                TFT.class,
                TTFT.class,
                TFTT.class
        ));
    }

    /**
     * Create Parameters object with supplied parameters.
     * @param k Cost of life
     * @param s Sucker's payoff
     * @param e Noise level
     * @param strategies List of strategies to use
     */
    public Parameters(double k, double s, double e, List<Class<? extends Strategy>> strategies) {
        this.k = k;
        this.s = s;
        this.e = e;
        this.strategies = strategies;
        this.l = 50;
        this.n = (int) (0.2 * Math.pow(this.l, 2));
        this.maxN = (int) (0.5 * Math.pow(this.l, 2));
        this.t = 5;
        this.r = 3;
        this.p = 0;
        this.evolution = false;
    }

    /**
     * Get the gride size.
     * @return Grid size
     */
    public int getL() {
        return l;
    }

    /**
     * Get the initial amount of agents.
     * @return Initial amount of agents
     */
    public int getN() {
        return n;
    }

    /**
     * Get the maximum amount of agents.
     * @return Maximum amount of agents
     */
    public int getMaxN() {
        return maxN;
    }

    /**
     * Get the cost of life.
     * @return Cost of life
     */
    public double getK() {
        return k;
    }

    /**
     * Set the cost of life.
     * @param k Cost of life
     */
    public void setK(double k) {
        this.k = k;
    }

    /**
     * Get the noise level.
     * @return Noise level
     */
    public double getE() {
        return e;
    }

    /**
     * Set the noise level.
     * @param e Noise level
     */
    public void setE(double e) {
        this.e = e;
    }

    /**
     * Get the temptation payoff.
     * @return Temptation payoff
     */
    public double getT() {
        return t;
    }

    /**
     * Get the reward payoff
     * @return Reward payoff
     */
    public double getR() {
        return r;
    }

    /**
     * Get the punishment payoff.
     * @return Punishment payoff
     */
    public double getP() {
        return p;
    }

    /**
     * Get the sucker's payoff.
     * @return Sucker's payoff
     */
    public double getS() {
        return s;
    }

    /**
     * Set the sucker's payoff.
     * @param s Sucker's payoff
     */
    public void setS(double s) {
        this.s = s;
    }

    public boolean getEvolution() {
        return evolution;
    }

    public void setEvolution(boolean evolution) {
        this.evolution = evolution;
    }

    /**
     * Get the list of strategies.
     * @return List of strategies
     */
    public List<Class<? extends Strategy>> getStrategies() {
        return strategies;
    }
}
