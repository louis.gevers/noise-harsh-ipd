package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Always cooperating strategy.
 */
public class ALLC extends Strategy {

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        return Action.COOPERATE;
    }
}
