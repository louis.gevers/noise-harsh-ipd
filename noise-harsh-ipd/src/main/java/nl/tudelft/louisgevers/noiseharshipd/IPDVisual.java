package nl.tudelft.louisgevers.noiseharshipd;

import nl.tudelft.louisgevers.noiseharshipd.agents.Player;
import nl.tudelft.louisgevers.noiseharshipd.visuals.PlayerPortrayal;
import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.grid.ObjectGridPortrayal2D;

import javax.swing.*;
import java.awt.*;

/**
 * Visualizes the spatial harsh IPD simulation.
 */
public class IPDVisual extends GUIState {

    /**
     * Main display to visualize the simulation.
     */
    private Display2D display;

    /**
     * Frame that contains the displays.
     */
    private JFrame displayFrame;

    /**
     * Visualization of the grid.
     */
    private ObjectGridPortrayal2D gridPortrayal;

    public IPDVisual() {
        super(new IPDTournament(System.currentTimeMillis()));
        this.gridPortrayal = new ObjectGridPortrayal2D();
    }

    /**
     * Initialize the GUI by constructing and attaching the displays.
     * @param controller Controller to register the displays at
     */
    public void init(Controller controller) {
        super.init(controller);
        display = new Display2D(600, 600, this);
        display.setClipping(false);

        displayFrame = display.createFrame();
        controller.registerFrame(displayFrame);
        displayFrame.setVisible(true);
        display.attach(gridPortrayal, "Grid");
    }

    /**
     * Called when start button is pressed.
     */
    public void start() {
        super.start();
        setupPortrayals();
    }

    /**
     * Called when simulation is loaded from checkpoint
     * @param state Checkpoint state
     */
    public void load(SimState state) {
        super.load(state);
        setupPortrayals();
    }

    /**
     * Called when GUI is about to get destroyed.
     */
    public void quit() {
        super.quit();
        if (displayFrame != null) {
            displayFrame.dispose();
        }
        displayFrame = null;
        display = null;
    }

    /**
     * Sets up the visualizations according to the simulation.
     */
    private void setupPortrayals() {
        IPDSimulation simulation = (IPDSimulation) state;

        gridPortrayal.setField(simulation.grid);
        gridPortrayal.setPortrayalForClass(Player.class, new PlayerPortrayal(simulation.strategies));

        display.reset();
        display.setBackdrop(Color.WHITE);
        display.repaint();
    }

    @Override
    public Object getSimulationInspectedObject() {
        return state;
    }

    /**
     * Start the simulation visualization.
     * @param args Console arguments
     */
    public static void main(String[] args) {
        IPDVisual vis = new IPDVisual();
        Console console = new Console(vis);
        console.setVisible(true);
    }

}
