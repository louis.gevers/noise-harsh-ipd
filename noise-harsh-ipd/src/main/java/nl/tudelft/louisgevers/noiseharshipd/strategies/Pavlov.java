package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Pavlov strategy.
 */
public class Pavlov extends Strategy {

    /**
     * Whether to cooperate.
     */
    boolean cooperate;

    /**
     * Initializes the strategy with initial cooperation.
     */
    public Pavlov() {
        this.cooperate = true;
    }

    @Override
    public Action play(Action lastMove, Action otherLastMove) {
        if (otherLastMove == null) {
            return Action.COOPERATE;
        } else {
            if (!lastMove.equals(otherLastMove)) {
                cooperate = !cooperate;
            }
            if (cooperate) {
                return Action.COOPERATE;
            } else {
                return Action.DEFECT;
            }
        }
    }
}
