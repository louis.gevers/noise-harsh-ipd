package nl.tudelft.louisgevers.noiseharshipd.util;

/**
 * Represents a 2D coordinate.
 */
public class Position {

    /**
     * X coordinate.
     */
    public int x;

    /**
     * Y coordinate.
     */
    public int y;

    /**
     * Creates 2D coordinate.
     * @param x X coordinate
     * @param y Y coordinate
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
