package nl.tudelft.louisgevers.noiseharshipd.strategies;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Contains list of strategies that can be used in the simulations.
 */
public class StrategiesCollection {

    /**
     * Immutable list of strategies.
     */
    private final List<Class<? extends Strategy>> strategies;

    /**
     * Maps strategies to their colors for visualizations.
     */
    private final Map<Class<? extends Strategy>, Color> colors;

    /**
     * Creates the immutable list of all strategies that can be used.
     */
    public StrategiesCollection() {
        List<Class<? extends Strategy>> strategies = new ArrayList<>(Arrays.asList(
                ALLC.class,
                ALLD.class,
                RAND.class,
                GRIM.class,
                TFT.class,
                STFT.class,
                GTFT.class,
                CTFT.class,
                Pavlov.class,
                GPavlov.class,
                TTFT.class,
                TFTT.class
        ));
        this.strategies = Collections.unmodifiableList(strategies);
        this.colors = new HashMap<>();
        createColors();
    }

    /**
     * Create immutable list of supplied strategies.
     * @param strategies List containing the desired strategies
     */
    public StrategiesCollection(List<Class<? extends Strategy>> strategies) {
        this.strategies = Collections.unmodifiableList(strategies);
        this.colors = new HashMap<>();
        createColors();
    }

    /**
     * Initialize colors for strategies.
     */
    private void createColors() {
        this.colors.put(ALLC.class, Color.GREEN);
        this.colors.put(ALLD.class, Color.RED);
        this.colors.put(RAND.class, Color.BLUE);
        this.colors.put(GRIM.class, Color.ORANGE);
        this.colors.put(TFT.class, Color.CYAN);
        this.colors.put(STFT.class, Color.DARK_GRAY);
        this.colors.put(GTFT.class, Color.MAGENTA);
        this.colors.put(CTFT.class, Color.PINK);
        this.colors.put(Pavlov.class, Color.LIGHT_GRAY);
        this.colors.put(GPavlov.class, Color.YELLOW);
        this.colors.put(TFTT.class, Color.GREEN.brighter());
        this.colors.put(TTFT.class, Color.GREEN.darker());
    }

    /**
     * Get the list of strategies.
     * @return Immutable list of strategies
     */
    public List<Class<? extends Strategy>> getStrategies() {
        return strategies;
    }

    /**
     * Returns the color for the given strategy.
     * @param strategy Strategy to determine the color of
     * @return Color of the strategy
     */
    public Color getColor(Class<? extends Strategy> strategy) {
        return this.colors.getOrDefault(strategy, Color.GRAY);
    }
}
