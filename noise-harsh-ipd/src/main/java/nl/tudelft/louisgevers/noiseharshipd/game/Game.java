package nl.tudelft.louisgevers.noiseharshipd.game;

import ec.util.MersenneTwisterFast;
import nl.tudelft.louisgevers.noiseharshipd.agents.Player;
import nl.tudelft.louisgevers.noiseharshipd.strategies.Strategy;
import nl.tudelft.louisgevers.noiseharshipd.util.Parameters;

import java.lang.reflect.InvocationTargetException;

/**
 * Represents an IPD game between two players.
 */
public class Game {

    /**
     * Parameters of the simulation.
     */
    private final Parameters parameters;

    /**
     * First player to play the game.
     */
    private final Player player1;

    /**
     * Second player to play the game.
     */
    private final Player player2;

    /**
     * Last move of the first player.
     */
    private Action lastMove1;

    /**
     * Last move of the second player.
     */
    private Action lastMove2;

    /**
     * Random number generator.
     */
    private final MersenneTwisterFast random;

    private Strategy strategy1;

    private Strategy strategy2;

    /**
     * Creates an IPD game instance for the two given players.
     * @param parameters Parameters of the simulation
     * @param player1 First player
     * @param player2 Second player
     * @param random Random number generator
     */
    public Game(Parameters parameters, Player player1, Player player2, MersenneTwisterFast random) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.parameters = parameters;
        this.player1 = player1;
        this.player2 = player2;
        this.strategy1 = player1.strategy.getConstructor().newInstance();
        this.strategy2 = player1.strategy.getConstructor().newInstance();
        this.random = random;
    }

    /**
     * Get the first player.
     * @return First player
     */
    public Player getPlayer1() {
        return player1;
    }

    /**
     * Get the second player
     * @return Second player
     */
    public Player getPlayer2() {
        return player2;
    }

    /**
     * Play one round of the Prisoner's Dilemma. Reward players after.
     */
    public void play() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if (!player1.strategy.equals(strategy1.getClass())) {
            strategy1 = player1.strategy.getConstructor().newInstance();
        }
        if (!player2.strategy.equals(strategy2.getClass())) {
            strategy2 = player2.strategy.getConstructor().newInstance();
        }
        Action action1 = playWithError(strategy1, lastMove1, lastMove2);
        Action action2 = playWithError(strategy2, lastMove2, lastMove1);
        final double R = parameters.getR();
        final double T = parameters.getT();
        final double S = parameters.getS();
        final double P = parameters.getP();
        switch (action1) {
            case COOPERATE:
                switch (action2) {
                    case COOPERATE:
                        rewardPlayers(R, R);
                        break;
                    case DEFECT:
                        rewardPlayers(S, T);
                        break;
                }
                break;
            case DEFECT:
                switch (action2) {
                    case COOPERATE:
                        rewardPlayers(T, S);
                        break;
                    case DEFECT:
                        rewardPlayers(P, P);
                        break;
                }
                break;
        }
        player1.played = true;
        player2.played = true;
        player1.cooperated = action1.equals(Action.COOPERATE);
        player2.cooperated = action2.equals(Action.COOPERATE);
        lastMove1 = action1;
        lastMove2 = action2;
    }

    /**
     * Get the chosen action of the player with possible error. I.e. given the error rate
     * as probability that the opposite action is returned.
     * @param strategy Strategy to make a decision
     * @param lastMove Player last move
     * @param otherLastMove Other player last move
     * @return The actual action, including possible result from error
     */
    private Action playWithError(Strategy strategy, Action lastMove, Action otherLastMove) {
        Action chosenAction = strategy.play(lastMove, otherLastMove);
        if (random.nextDouble() < parameters.getE()) {
            if (chosenAction.equals(Action.COOPERATE)) {
                return Action.DEFECT;
            } else {
                return Action.COOPERATE;
            }
        } else {
            return chosenAction;
        }
    }

    /**
     * Reward players by adding the given rewards to the players' energy.
     * @param reward1 Reward for the first player
     * @param reward2 Reward for the second player
     */
    private void rewardPlayers(double reward1, double reward2) {
        player1.addEnergy(reward1);
        player2.addEnergy(reward2);
    }

}
