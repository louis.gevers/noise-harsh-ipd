package nl.tudelft.louisgevers.noiseharshipd.environment;

import ec.util.MersenneTwisterFast;
import nl.tudelft.louisgevers.noiseharshipd.agents.Player;
import nl.tudelft.louisgevers.noiseharshipd.util.Parameters;
import nl.tudelft.louisgevers.noiseharshipd.util.Position;
import sim.field.grid.Grid2D;
import sim.field.grid.ObjectGrid2D;
import sim.util.Bag;
import sim.util.IntBag;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Represents a 2-dimensional grid with helper methods for spatial IPD.
 */
public class Grid extends ObjectGrid2D {

    /**
     * Creates the grid with fixed width and height.
     */
    public Grid(Parameters parameters) {
        super(parameters.getL(), parameters.getL());
    }

    /**
     * Sets the current object at a random, empty location. Only works if the grid
     * is not already full.
     * @param player Player to add
     * @param random Random number generator
     */
    public void setAtRandomLocation(Player player, MersenneTwisterFast random) {
        boolean foundLocation = false;
        int maxLocations = this.width * this.height;
        int attempts = 0;
        while (!foundLocation && attempts < maxLocations) {
            int x = random.nextInt(this.width);
            int y = random.nextInt(this.height);
            if (isAvailable(x, y)) {
                this.set(x, y, player);
                player.position.x = x;
                player.position.y = y;
                foundLocation = true;
            } else {
                attempts++;
            }
        }
    }

    /**
     * Get all the neighbors (8 cells around player) of the given player.
     * @param player Player to get the neighbors of
     * @return Bag of neighbors of the given player
     */
    public Bag getNeighbors(Player player) {
        final int distance = 1;
        final boolean includeOrigin = false;
        return getMooreNeighbors(player.position.x, player.position.y, distance, Grid2D.TOROIDAL, includeOrigin);
    }

    /**
     * Get a random available position around the given player. If no such position exists,
     * i.e. all neighboring cells are occupied, null is returned.
     * @param player Player to look for neighorhood
     * @param random Random number generator
     * @return Available position or null
     */
    public Position getRandomAvailableCell(Player player, MersenneTwisterFast random) {
        List<Position> positions = getAvailableCells(player);
        if (positions.size() > 0) {
            int i = random.nextInt(positions.size());
            return positions.get(i);
        } else {
            return null;
        }
    }

    /**
     * Moves the given player in a random direction, given that one cell is available.
     * @param player Player to move
     * @param random Random number generator
     */
    public void randomMove(Player player, MersenneTwisterFast random) {
        Position newPosition = getRandomAvailableCell(player, random);
        if (newPosition != null) {
            movePlayer(player, newPosition);
        }
    }

    /**
     * Returns whether the cell at given coordinate is available, i.e. no
     * agents occupy it.
     * @param x X coordinate of the cell to check
     * @param y Y coordinate of the cell to check
     * @return Whether the cell at given coordinate contains no agent
     */
    private boolean isAvailable(int x, int y) {
        return this.get(x, y) == null;
    }

    /**
     * Moves the given player to the new position.
     * @param player Player to move
     * @param newPosition New position to move to
     */
    private void movePlayer(Player player, Position newPosition) {
        set(player.position.x, player.position.y, null);
        set(newPosition.x, newPosition.y, player);
        player.position.x = newPosition.x;
        player.position.y = newPosition.y;
    }

    /**
     * Get a list of positions in the neighborhood of the given player that
     * are empty.
     * @param player Player to look for neighborhood
     * @return List of available positions around the player
     */
    private List<Position> getAvailableCells(Player player) {
        Bag result = new Bag();
        IntBag xs = new IntBag();
        IntBag ys = new IntBag();

        final int distance = 1;
        final boolean includeOrigin = false;
        getMooreNeighbors(player.position.x, player.position.y, distance, Grid2D.TOROIDAL, includeOrigin, result, xs, ys);

        return IntStream.range(0, xs.size())
                .mapToObj(i -> new Position(xs.get(i), ys.get(i)))
                .filter(pos -> isAvailable(pos.x, pos.y))
                .collect(Collectors.toList());
    }
}
