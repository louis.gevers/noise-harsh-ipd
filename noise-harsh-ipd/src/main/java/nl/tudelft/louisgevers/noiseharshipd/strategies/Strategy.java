package nl.tudelft.louisgevers.noiseharshipd.strategies;

import nl.tudelft.louisgevers.noiseharshipd.game.Action;

/**
 * Represents a strategy in IPD game.
 */
public abstract class Strategy {

    /**
     * Determine which action to take given the two players' action history
     * @param lastMove Own last move
     * @param otherLastMove Other player's last move
     * @return The taken action
     */
    public abstract Action play(Action lastMove, Action otherLastMove);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
