package nl.tudelft.louisgevers.noiseharshipd;

/**
 * Extension class for tournaments. Gives the counts of every strategy.
 */
public class IPDTournament extends IPDSimulation {

    /**
     * Constructor of IPD tournament.
     * @param seed Seed for random number generator.
     */
    public IPDTournament(long seed) {
        super(seed);
    }

    /**
     * Get amount of ALLC strategies in the game.
     * @return Amount of ALLC strategies
     */
    public int getALLCCount() {
        return strategiesCount.getOrDefault("ALLC", 0);
    }

    /**
     * Get amount of ALLD strategies in the game.
     * @return Amount of ALLD strategies
     */
    public int getALLDCount() {
        return strategiesCount.getOrDefault("ALLD", 0);
    }

    /**
     * Get amount of CTFT strategies in the game.
     * @return Amount of CTFT strategies
     */
    public int getCTFTCount() {
        return strategiesCount.getOrDefault("CTFT", 0);
    }

    /**
     * Get amount of GPavlov strategies in the game.
     * @return Amount of GPavlov strategies
     */
    public int getGPavlovCount() {
        return strategiesCount.getOrDefault("GPavlov", 0);
    }

    /**
     * Get amount of SGPavlov strategies in the game.
     * @return Amount of SGPavlov strategies
     */
    public int getSGPavlovCount() {
        return strategiesCount.getOrDefault("SGPavlov", 0);
    }

    /**
     * Get amount of GRIM strategies in the game.
     * @return Amount of GRIM strategies
     */
    public int getGRIMCount() {
        return strategiesCount.getOrDefault("GRIM", 0);
    }

    /**
     * Get amount of GTFT strategies in the game.
     * @return Amount of GTFT strategies
     */
    public int getGTFTCount() {
        return strategiesCount.getOrDefault("GTFT", 0);
    }

    /**
     * Get amount of SGTFT strategies in the game.
     * @return Amount of SGTFT strategies
     */
    public int getSGTFTCount() {
        return strategiesCount.getOrDefault("SGTFT", 0);
    }

    /**
     * Get amount of Pavlov strategies in the game.
     * @return Amount of Pavlov strategies
     */
    public int getPavlovCount() {
        return strategiesCount.getOrDefault("Pavlov", 0);
    }

    /**
     * Get amount of RAND strategies in the game.
     * @return Amount of RAND strategies
     */
    public int getRANDCount() {
        return strategiesCount.getOrDefault("RAND", 0);
    }

    /**
     * Get amount of STFT strategies in the game.
     * @return Amount of STFT strategies
     */
    public int getSTFTCount() {
        return strategiesCount.getOrDefault("STFT", 0);
    }

    /**
     * Get amount of TFT strategies in the game.
     * @return Amount of TFT strategies
     */
    public int getTFTCount() {
        return strategiesCount.getOrDefault("TFT", 0);
    }

    /**
     * Get amount of TFTT strategies in the game.
     * @return Amount of TFTT strategies
     */
    public int getTFTTCount() {
        return strategiesCount.getOrDefault("TFTT", 0);
    }

    /**
     * Get amount of TTFT strategies in the game.
     * @return Amount of TTFT strategies
     */
    public int getTTFTCount() {
        return strategiesCount.getOrDefault("TTFT", 0);
    }

}
